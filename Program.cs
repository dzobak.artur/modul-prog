﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace modul1
{
    [Serializable]
    public class HotelRoom
    {
        public int RoomNumber { get; set; }
        public int Floor { get; set; }
        public int Capacity { get; set; }
        public decimal DailyRate { get; set; }
    }

    public class Tenant
    {
        public string LastName { get; set; }
        public int RoomNumber { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            List<HotelRoom> hotelRooms = new List<HotelRoom>
            {
                new HotelRoom { RoomNumber = 1, Floor = 1, Capacity = 2, DailyRate = 100 },
                new HotelRoom { RoomNumber = 2, Floor = 2, Capacity = 3, DailyRate = 150 },
            };

            List<Tenant> tenants = new List<Tenant>
            {
                new Tenant { LastName = "Smith", RoomNumber = 1, CheckInDate = new DateTime(2023, 10, 1), CheckOutDate = new DateTime(2023, 11, 5) },
                new Tenant { LastName = "Johnson", RoomNumber = 2, CheckInDate = new DateTime(2023, 10, 3), CheckOutDate = new DateTime(2023, 11, 27) },
            };

            DateTime desiredDate = new DateTime(2023, 10, 3); 

            var tenantsOnDesiredDate = tenants
                .Where(tenant => tenant.CheckInDate <= desiredDate && tenant.CheckOutDate >= desiredDate)
                .Select(tenant => tenant.LastName);

            Console.WriteLine($"Прізвища жильців, які проживали на {desiredDate.ToString("yyyy-MM-dd")}:");
            Console.WriteLine(string.Join(", ", tenantsOnDesiredDate));


            var totalDailyRevenueByFloor = tenants
                .Where(tenant => tenant.CheckOutDate > DateTime.Now)
                .GroupBy(tenant => hotelRooms.First(room => room.RoomNumber == tenant.RoomNumber).Floor)
                .Select(group => new
                {
                    Floor = group.Key,
                    TotalRevenue = group.Sum(tenant =>
                    {
                        var room = hotelRooms.First(r => r.RoomNumber == tenant.RoomNumber);
                        return (tenant.CheckOutDate - tenant.CheckInDate).Days * room.DailyRate;
                    })
                });

            Console.WriteLine("Сумарна грошова сума за добу для кожного поверху:");
            totalDailyRevenueByFloor.ToList().ForEach(data =>
            {
                Console.WriteLine($"Поверх {data.Floor}: {data.TotalRevenue:C}");
            });

            XmlSerializer serializer = new XmlSerializer(typeof(List<HotelRoom>));
            using (StreamWriter writer = new StreamWriter("hotelData.xml"))
            {
                serializer.Serialize(writer, hotelRooms);
            }

            Console.WriteLine("Дані готелю були збережені в hotelData.xml");
        }
    }
}
